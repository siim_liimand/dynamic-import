import { VNode } from 'preact';
export interface ComponentProps {
    children?: VNode<any> | null;
}
export declare type Component = VNode<ComponentProps>;
export declare type DynamicImportComponent = () => Component;
export declare type ResolveImport = (component: Component) => void;
export declare type GetComponentCallback = (resolve: ResolveImport) => void;
export declare type SetComponentCallback = (setComponent: ResolveImport) => void;
export declare type DynamicImport = (key: string, getComponent: GetComponentCallback, setComponentCallback?: SetComponentCallback) => DynamicImportComponent;
export declare type ImportableItem = [key: string, getComponent: GetComponentCallback, setImportedComponent?: ResolveImport];
export declare type ImportedItem = [key: string, component: Component];
export declare const isImported: () => boolean;
export declare const dynamicImport: DynamicImport;
//# sourceMappingURL=index.d.ts.map