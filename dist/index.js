"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dynamicImport = exports.isImported = void 0;
const preact_1 = require("preact");
const hooks_1 = require("preact/hooks");
const importable = [];
const imported = [];
let importing;
let repetitions = 0;
const wait = (callback) => {
    repetitions++;
    if (repetitions < 5) {
        setTimeout(callback, 10);
    }
    else {
        importing = false;
    }
};
const importItems = () => {
    const item = importable.shift();
    if (item !== undefined) {
        const [key, getComponentCallback, setImportedComponent] = item;
        const resolve = (component) => {
            imported.push([key, component]);
            if (setImportedComponent !== undefined) {
                setImportedComponent(component);
            }
            importItems();
        };
        getComponentCallback(resolve);
    }
    else {
        wait(importItems);
    }
};
const addImportableItem = (item) => {
    importable.push(item);
    if (!importing) {
        importing = true;
        importItems();
    }
};
const getImportedItem = (key) => {
    const importedItem = imported.find(([k]) => key === k);
    if (importedItem) {
        return importedItem[1];
    }
};
const isImported = () => importing === false;
exports.isImported = isImported;
const dynamicImport = (key, getComponentCallback, setComponentCallback) => {
    const DynamicImportComponent = () => {
        const initialComponent = (0, preact_1.createElement)(preact_1.Fragment, {});
        const importedItem = getImportedItem(key);
        const [component, setComponent] = (0, hooks_1.useState)(importedItem || initialComponent);
        const [loaded, setLoaded] = (0, hooks_1.useState)(false);
        const setComponentHandler = (comp) => {
            loaded && setComponent(comp);
        };
        setComponentCallback && setComponentCallback(setComponentHandler);
        if (importedItem === undefined) {
            if (typeof window === 'undefined') {
                addImportableItem([key, getComponentCallback]);
            }
            else {
                addImportableItem([key, getComponentCallback, setComponent]);
            }
        }
        (0, hooks_1.useEffect)(() => {
            setLoaded(true);
            return () => {
                setLoaded(false);
            };
        }, []);
        return component;
    };
    return DynamicImportComponent;
};
exports.dynamicImport = dynamicImport;
//# sourceMappingURL=index.js.map