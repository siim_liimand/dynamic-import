import { createElement, Fragment, VNode } from 'preact';
import { useEffect, useState } from 'preact/hooks';

export interface ComponentProps {
  children?: VNode<any> | null;
}
export type Component = VNode<ComponentProps>;
export type DynamicImportComponent = () => Component;
export type ResolveImport = (component: Component) => void;
export type GetComponentCallback = (resolve: ResolveImport) => void;
export type SetComponentCallback = (setComponent: ResolveImport) => void;
export type DynamicImport = (
  key: string,
  getComponent: GetComponentCallback,
  setComponentCallback?: SetComponentCallback,
) => DynamicImportComponent;
export type ImportableItem = [key: string, getComponent: GetComponentCallback, setImportedComponent?: ResolveImport];
export type ImportedItem = [key: string, component: Component];

const importable: ImportableItem[] = [];
const imported: ImportedItem[] = [];
let importing: boolean | undefined;
let repetitions: number = 0;

const wait = (callback: () => void): void => {
  repetitions++;
  if (repetitions < 5) {
    setTimeout(callback, 10);
  } else {
    importing = false;
  }
};

const importItems = (): void => {
  const item: ImportableItem | undefined = importable.shift();
  if (item !== undefined) {
    const [key, getComponentCallback, setImportedComponent] = item;
    const resolve: ResolveImport = (component): void => {
      imported.push([key, component]);
      if (setImportedComponent !== undefined) {
        setImportedComponent(component);
      }
      importItems();
    };
    getComponentCallback(resolve);
  } else {
    wait(importItems);
  }
};

const addImportableItem = (item: ImportableItem): void => {
  importable.push(item);
  if (!importing) {
    importing = true;
    importItems();
  }
};

const getImportedItem = (key: string): Component | undefined => {
  const importedItem = imported.find(([k]) => key === k);
  if (importedItem) {
    return importedItem[1];
  }
};

export const isImported = (): boolean => importing === false;

export const dynamicImport: DynamicImport = (key, getComponentCallback, setComponentCallback) => {
  const DynamicImportComponent: DynamicImportComponent = () => {
    const initialComponent = createElement(Fragment, {});
    const importedItem = getImportedItem(key);
    const [component, setComponent] = useState<Component>(importedItem || initialComponent);
    const [loaded, setLoaded] = useState<boolean>(false);
    const setComponentHandler = (comp: Component) => {
      loaded && setComponent(comp);
    };

    setComponentCallback && setComponentCallback(setComponentHandler);

    if (importedItem === undefined) {
      if (typeof window === 'undefined') {
        addImportableItem([key, getComponentCallback]);
      } else {
        addImportableItem([key, getComponentCallback, setComponent]);
      }
    }

    useEffect(() => {
      setLoaded(true);
      return () => {
        setLoaded(false);
      };
    }, []);

    return component;
  };

  return DynamicImportComponent;
};
